#!/bin/bash
################################################################################
# Author: SYSWEB
#-------------------------------------------------------------------------------
# Ce script va installer Odoo sur votre serveur Ubuntu. Il peut installer plusieurs instances d'Odoo
#-------------------------------------------------------------------------------
# Comment faire ?
# Télécharger le fichier à la racine du serveur
# sudo nano odoo-install.sh
# Placez-y ce contenu et rendez ensuite le fichier exécutable :
# sudo chmod +x odoo-install.sh
# Exécutez le script pour installer Odoo :
# ./odoo-install
################################################################################

OE_USER="odoo"
OE_HOME="/$OE_USER"
OE_HOME_EXT="/$OE_USER/${OE_USER}-server"
#WKHTMLTOPDF
INSTALL_WKHTMLTOPDF="True"
# Port ODOO
OE_PORT="8069"
# Choisissez la version d'Odoo que vous voulez installer. Par exemple : 13.0, 12.0, 11.0 ou saas-18. Si vous utilisez la version master, la version master sera installée.
# IMPORTANT ! Ce script contient des bibliothèques supplémentaires qui sont spécifiquement nécessaires pour Odoo 13.0
OE_VERSION="13.0"
# Mettez cette option sur True si vous voulez installer la version entreprise d'Odoo.
IS_ENTERPRISE="False"
# Mettez cette option sur True si vous voulez installer Nginx !
INSTALL_NGINX="True"
# Définissez le mot de passe superadmin. En revanche si GENERATE_RANDOM_PASSWORD est réglé sur True. Alors nous générerons automatiquement un mot de passe aléatoire, dans le cas contraire nous utiliserons celui-ci
OE_SUPERADMIN="admin"
# Mettre à "True" pour générer un mot de passe aléatoire, False pour utiliser la variable dans OE_SUPERADMIN
GENERATE_RANDOM_PASSWORD="True"
OE_CONFIG="${OE_USER}-server"
# Définir le nom du site web
WEBSITE_NAME="_"
# Set the default Odoo longpolling port (you still have to use -c /etc/odoo-server.conf for example to use this.)
LONGPOLLING_PORT="8072"
# Mettre à True pour installer certbot et avoir le ssl actif, False pour aucun SSL
ENABLE_SSL="True"
# Un e-mail pour enregistrer le certificat ssl
ADMIN_EMAIL="odoo@example.com"
##
###  WKHTMLTOPDF download links
## === Ubuntu Trusty x64 & x32 === (for other distributions please replace these two links,
## in order to have correct version of wkhtmltopdf installed, for a danger note refer to
## https://github.com/odoo/odoo/wiki/Wkhtmltopdf ):
## https://www.odoo.com/documentation/12.0/setup/install.html#debian-ubuntu

WKHTMLTOX_X64=https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.5/wkhtmltox_0.12.5-1.trusty_amd64.deb
WKHTMLTOX_X32=https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.5/wkhtmltox_0.12.5-1.trusty_i386.deb
#--------------------------------------------------
# Mise à jour du serveur
#--------------------------------------------------

echo -e "\n---- Mise à jour des paquets ----"
sudo apt update
echo -e "\n---- Installation des software-properties-common ----"
sudo apt install software-properties-common -y
echo -e "\n---- Mise à jour des repo ----"
sudo add-apt-repository "deb http://mirrors.kernel.org/ubuntu/ xenial main"
sudo add-apt-repository universe
echo -e "\n---- Mise à jour du serveur ----"
sudo apt upgrade -y

#--------------------------------------------------
# Install PostgreSQL Server
#--------------------------------------------------

echo -e "\n---- Installation du serveur PostgreSQL ----"
sudo apt install postgresql postgresql-server-dev-all -y

echo -e "\n---- Création de l\'utilisateur ODOO PostgreSQL  ----"
sudo su - postgres -c "createuser -s $OE_USER" 2> /dev/null || true

#--------------------------------------------------
# Installer des dépendances
#--------------------------------------------------
echo -e "\n--- Installation de Python 3 + pip3 --"
sudo apt install git python3 python3-pip build-essential wget python3-dev python3-venv python3-wheel libxslt-dev libzip-dev libldap2-dev libsasl2-dev python3-setuptools node-less libpng12-0 gdebi -y

echo -e "\n---- Installation des paquets/exigences python ----"
sudo -H pip3 install -r https://github.com/odoo/odoo/raw/${OE_VERSION}/requirements.txt

echo -e "\n---- Installation de nodeJS NPM et rtlcss pour le support LTR ----"
sudo apt install nodejs npm -y
sudo npm install -g rtlcss

#--------------------------------------------------
# Installation de Wkhtmltopdf si nécessaire
#--------------------------------------------------
if [ $INSTALL_WKHTMLTOPDF = "True" ]; then
  echo -e "\n---- Installez wkhtml et placez les raccourcis au bon endroit pour ODOO 13 ----"
  #en choisir une correcte parmi les versions x64 et x32 :
  if [ "`getconf LONG_BIT`" == "64" ];then
      _url=$WKHTMLTOX_X64
  else
      _url=$WKHTMLTOX_X32
  fi
  sudo wget $_url
  sudo gdebi --n `basename $_url`
  sudo ln -s /usr/local/bin/wkhtmltopdf /usr/bin
  sudo ln -s /usr/local/bin/wkhtmltoimage /usr/bin
else
  echo "Wkhtmltopdf n'est pas installé en raison du choix de l'utilisateur!"
fi

echo -e "\n---- Création d'un utilisateur du système ODOO ----"
sudo adduser --system --quiet --shell=/bin/bash --home=$OE_HOME --gecos 'ODOO' --group $OE_USER
#L'utilisateur doit également être ajouté au groupe sudo.
sudo adduser $OE_USER sudo

echo -e "\n---- Création d'un répertoire des historiques ----"
sudo mkdir /var/log/$OE_USER
sudo chown $OE_USER:$OE_USER /var/log/$OE_USER

#--------------------------------------------------
# Installation d'ODOO
#--------------------------------------------------
echo -e "\n==== Installation du serveur ODOO ===="
sudo git clone --depth 1 --branch $OE_VERSION https://www.github.com/odoo/odoo $OE_HOME_EXT/

if [ $IS_ENTERPRISE = "True" ]; then
    # Installation d'Odoo Enterprise !
    echo -e "\n--- Création d'un lien symbolique pour le nœud"
    sudo ln -s /usr/bin/nodejs /usr/bin/node
    sudo su $OE_USER -c "mkdir $OE_HOME/enterprise"
    sudo su $OE_USER -c "mkdir $OE_HOME/enterprise/addons"

    GITHUB_RESPONSE=$(sudo git clone --depth 1 --branch $OE_VERSION https://www.github.com/odoo/enterprise "$OE_HOME/enterprise/addons" 2>&1)
    while [[ $GITHUB_RESPONSE == *"Authentication"* ]]; do
        echo "------------------------! ATTENTION !------------------------------"
        echo "Votre authentification auprès de Github a échoué ! Veuillez réessayer."
        printf "In order to clone and install the Odoo enterprise version you \nneed to be an offical Odoo partner and you need access to\nhttp://github.com/odoo/enterprise.\n"
        echo "ASTUCE : appuyez sur ctrl+c pour arrêter le script."
        echo "-------------------------------------------------------------"
        echo " "
        GITHUB_RESPONSE=$(sudo git clone --depth 1 --branch $OE_VERSION https://www.github.com/odoo/enterprise "$OE_HOME/enterprise/addons" 2>&1)
    done

    echo -e "\n---- Ajout du code d'entreprise ici : $OE_HOME/enterprise/addons ----"
    echo -e "\n---- Installation des bibliothèques spécifiques ODOO Entreprises ----"
    sudo -H pip3 install num2words ofxparse dbfread ebaysdk firebase_admin pyOpenSSL
    sudo npm install -g less
    sudo npm install -g less-plugin-clean-css
fi

echo -e "\n---- Création d'un répertoire pour les modules personnalisés ----"
sudo su $OE_USER -c "mkdir $OE_HOME/custom"
sudo su $OE_USER -c "mkdir $OE_HOME/custom/addons"

echo -e "\n---- Définition des autorisations pour le dossier principal ----"
sudo chown -R $OE_USER:$OE_USER $OE_HOME/*

echo -e "* Création d'un fichier de configuration du serveur"


sudo touch /etc/${OE_CONFIG}.conf
echo -e "* Création du fichier de configuration du serveur"
sudo su root -c "printf '[options] \n; Voici le mot de passe de la base de données :\n' >> /etc/${OE_CONFIG}.conf"
if [ $GENERATE_RANDOM_PASSWORD = "True" ]; then
    echo -e "* Génération d'un mot de passe admin aléatoire"
    OE_SUPERADMIN=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 16 | head -n 1)
fi
sudo su root -c "printf 'admin_passwd = ${OE_SUPERADMIN}\n' >> /etc/${OE_CONFIG}.conf"
if [ $OE_VERSION > "11.0" ];then
    sudo su root -c "printf 'http_port = ${OE_PORT}\n' >> /etc/${OE_CONFIG}.conf"
else
    sudo su root -c "printf 'xmlrpc_port = ${OE_PORT}\n' >> /etc/${OE_CONFIG}.conf"
fi
sudo su root -c "printf 'logfile = /var/log/${OE_USER}/${OE_CONFIG}.log\n' >> /etc/${OE_CONFIG}.conf"

if [ $IS_ENTERPRISE = "True" ]; then
    sudo su root -c "printf 'addons_path=${OE_HOME}/enterprise/addons,${OE_HOME_EXT}/addons\n' >> /etc/${OE_CONFIG}.conf"
else
    sudo su root -c "printf 'addons_path=${OE_HOME_EXT}/addons,${OE_HOME}/custom/addons\n' >> /etc/${OE_CONFIG}.conf"
fi
sudo chown $OE_USER:$OE_USER /etc/${OE_CONFIG}.conf
sudo chmod 640 /etc/${OE_CONFIG}.conf

echo -e "* Création d'un fichier de lancement"
sudo su root -c "echo '#!/bin/sh' >> $OE_HOME_EXT/start.sh"
sudo su root -c "echo 'sudo -u $OE_USER $OE_HOME_EXT/odoo-bin --config=/etc/${OE_CONFIG}.conf' >> $OE_HOME_EXT/start.sh"
sudo chmod 755 $OE_HOME_EXT/start.sh

#--------------------------------------------------
# Ajout de ODOO en tant que deamon (initscript)
#--------------------------------------------------

echo -e "* Création d'un fichier init"
cat <<EOF > ~/$OE_CONFIG
#!/bin/sh
### DÉBUT INFO INIT
# Propose: $OE_CONFIG
# Required-Start: \$remote_fs \$syslog
# Required-Stop: \$remote_fs \$syslog
# Should-Start: \$network
# Should-Stop: \$network
# Default-Start: 2 3 4 5
# Default-Stop: 0 1 6
# Short-Description: Odoo et SYSWEB est une applications pour les entreprises
# Description: Odoo et SYSWEB
### END INIT INFO
PATH=/sbin:/bin:/usr/sbin:/usr/bin:/usr/local/bin
DAEMON=$OE_HOME_EXT/odoo-bin
NAME=$OE_CONFIG
DESC=$OE_CONFIG
# Specify the user name (Default: odoo).
USER=$OE_USER
# Specify an alternate config file (Default: /etc/openerp-server.conf).
CONFIGFILE="/etc/${OE_CONFIG}.conf"
# pidfile
PIDFILE=/var/run/\${NAME}.pid
# Additional options that are passed to the Daemon.
DAEMON_OPTS="-c \$CONFIGFILE"
[ -x \$DAEMON ] || exit 0
[ -f \$CONFIGFILE ] || exit 0
checkpid() {
[ -f \$PIDFILE ] || return 1
pid=\`cat \$PIDFILE\`
[ -d /proc/\$pid ] && return 0
return 1
}
case "\${1}" in
start)
echo -n "Starting \${DESC}: "
start-stop-daemon --start --quiet --pidfile \$PIDFILE \
--chuid \$USER --background --make-pidfile \
--exec \$DAEMON -- \$DAEMON_OPTS
echo "\${NAME}."
;;
stop)
echo -n "Stopping \${DESC}: "
start-stop-daemon --stop --quiet --pidfile \$PIDFILE \
--oknodo
echo "\${NAME}."
;;
restart|force-reload)
echo -n "Restarting \${DESC}: "
start-stop-daemon --stop --quiet --pidfile \$PIDFILE \
--oknodo
sleep 1
start-stop-daemon --start --quiet --pidfile \$PIDFILE \
--chuid \$USER --background --make-pidfile \
--exec \$DAEMON -- \$DAEMON_OPTS
echo "\${NAME}."
;;
*)
N=/etc/init.d/\$NAME
echo "Usage: \$NAME {start|stop|restart|force-reload}" >&2
exit 1
;;
esac
exit 0
EOF

echo -e "* Fichier de sécurité Init"
sudo mv ~/$OE_CONFIG /etc/init.d/$OE_CONFIG
sudo chmod 755 /etc/init.d/$OE_CONFIG
sudo chown root: /etc/init.d/$OE_CONFIG

echo -e "* Démarrer ODOO au démarrage du serveur"
sudo update-rc.d $OE_CONFIG defaults

#--------------------------------------------------
# Installez Nginx si nécessaire
#--------------------------------------------------
if [ $INSTALL_NGINX = "True" ]; then
  echo -e "\n---- Installation et mise en place de Nginx ----"
  sudo apt install nginx -y
  cat <<EOF > ~/odoo
  server {
    listen 80;
    listen 443 ssl http2;
    server_name $WEBSITE_NAME;
  #   Fichier de configuration par sysweb

  # Add Headers for odoo proxy mode
  proxy_set_header X-Forwarded-Host \$host;
  proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
  proxy_set_header X-Forwarded-Proto \$scheme;
  proxy_set_header X-Real-IP \$remote_addr;
  add_header X-Frame-Options "SAMEORIGIN";
  add_header X-XSS-Protection "1; mode=block";
  proxy_set_header X-Client-IP \$remote_addr;
  proxy_set_header HTTP_X_FORWARDED_HOST \$remote_addr;


  #HSTS
  add_header Strict-Transport-Security "max-age=31536000" always;

  #   odoo    log files
  access_log  /var/log/nginx/$OE_USER-access.log;
  error_log       /var/log/nginx/$OE_USER-error.log;

  #   increase    proxy   buffer  size
  proxy_buffers   16  64k;
  proxy_buffer_size   128k;
  proxy_read_timeout 900s;
  proxy_connect_timeout 900s;
  proxy_send_timeout 900s;
  #   force   timeouts    if  the backend dies
  proxy_next_upstream error   timeout invalid_header  http_500    http_502
  http_503;
  types {
  text/less less;
  text/scss scss;
  }
  #   enable  data    compression
  gzip    on;
  gzip_min_length 1100;
  gzip_buffers    4   32k;
  gzip_types  text/css text/less text/plain text/xml application/xml application/json application/javascript application/pdf image/jpeg image/png;
  gzip_vary   on;
  client_header_buffer_size 4k;
  large_client_header_buffers 4 64k;
  client_max_body_size 0;
  location / {
  proxy_pass    http://127.0.0.1:$OE_PORT;
  # by default, do not forward anything
  proxy_redirect off;
  }
  location /longpolling {
  proxy_pass http://127.0.0.1:$LONGPOLLING_PORT;
  }
  location ~* .(js|css|png|jpg|jpeg|gif|ico)$ {
  expires 2d;
  proxy_pass http://127.0.0.1:$OE_PORT;
  add_header Cache-Control "public, no-transform";
  }
  # cache some static data in memory for 60mins.
  location ~ /[a-zA-Z0-9_-]*/static/ {
  proxy_cache_valid 200 302 60m;
  proxy_cache_valid 404      1m;
  proxy_buffering    on;
  expires 864000;
  proxy_pass    http://127.0.0.1:$OE_PORT;
  }
  }
EOF

  sudo mv ~/odoo /etc/nginx/sites-available/
  sudo ln -s /etc/nginx/sites-available/odoo /etc/nginx/sites-enabled/odoo
  sudo rm /etc/nginx/sites-enabled/default
  sudo service nginx reload
  sudo su root -c "printf 'proxy_mode = True\n' >> /etc/${OE_CONFIG}.conf"
  echo "C'est fait ! Le serveur Nginx est en place et fonctionne. La configuration peut être trouvée à l'adresse /etc/nginx/sites-available/odoo"
else
  echo "Nginx n'est pas installé en raison du choix de l'utilisateur!"
fi

#--------------------------------------------------
# Activer ssl avec certbot
#--------------------------------------------------

if [ $INSTALL_NGINX = "True" ] && [ $ENABLE_SSL = "True" ] && [ $ADMIN_EMAIL != "odoo@example.com" ]  && [ $WEBSITE_NAME != "_" ];then
  sudo add-apt-repository ppa:certbot/certbot -y && sudo apt update -y
  sudo apt install python-certbot-nginx -y
  sudo certbot --nginx -d $WEBSITE_NAME --noninteractive --agree-tos --email $ADMIN_EMAIL --redirect
  sudo service nginx reload
  echo "SSL/HTTPS est activé!"
else
  echo "Le SSL/HTTPS n'est pas activé en raison du choix de l'utilisateur ou d'une mauvaise configuration !"
fi

echo -e "* Démarrage du service Odoo"
sudo su root -c "/etc/init.d/$OE_CONFIG start"
echo "-----------------------------------------------------------"
echo "          _____            _____                   _____                   _____                   _____                   _____          ";
echo "         /\    \          |\    \                 /\    \                 /\    \                 /\    \                 /\    \         ";
echo "        /::\    \         |:\____\               /::\    \               /::\____\               /::\    \               /::\    \        ";
echo "       /::::\    \        |::|   |              /::::\    \             /:::/    /              /::::\    \             /::::\    \       ";
echo "      /::::::\    \       |::|   |             /::::::\    \           /:::/   _/___           /::::::\    \           /::::::\    \      ";
echo "     /:::/\:::\    \      |::|   |            /:::/\:::\    \         /:::/   /\    \         /:::/\:::\    \         /:::/\:::\    \     ";
echo "    /:::/__\:::\    \     |::|   |           /:::/__\:::\    \       /:::/   /::\____\       /:::/__\:::\    \       /:::/__\:::\    \    ";
echo "    \:::\   \:::\    \    |::|   |           \:::\   \:::\    \     /:::/   /:::/    /      /::::\   \:::\    \     /::::\   \:::\    \   ";
echo "  ___\:::\   \:::\    \   |::|___|______   ___\:::\   \:::\    \   /:::/   /:::/   _/___   /::::::\   \:::\    \   /::::::\   \:::\    \  ";
echo " /\   \:::\   \:::\    \  /::::::::\    \ /\   \:::\   \:::\    \ /:::/___/:::/   /\    \ /:::/\:::\   \:::\    \ /:::/\:::\   \:::\ ___\ ";
echo "/::\   \:::\   \:::\____\/::::::::::\____/::\   \:::\   \:::\____|:::|   /:::/   /::\____/:::/__\:::\   \:::\____/:::/__\:::\   \:::|    |";
echo "\:::\   \:::\   \::/    /:::/~~~~/~~     \:::\   \:::\   \::/    |:::|__/:::/   /:::/    \:::\   \:::\   \::/    \:::\   \:::\  /:::|____|";
echo " \:::\   \:::\   \/____/:::/    /         \:::\   \:::\   \/____/ \:::\/:::/   /:::/    / \:::\   \:::\   \/____/ \:::\   \:::\/:::/    / ";
echo "  \:::\   \:::\    \  /:::/    /           \:::\   \:::\    \      \::::::/   /:::/    /   \:::\   \:::\    \      \:::\   \::::::/    /  ";
echo "   \:::\   \:::\____\/:::/    /             \:::\   \:::\____\      \::::/___/:::/    /     \:::\   \:::\____\      \:::\   \::::/    /   ";
echo "    \:::\  /:::/    /\::/    /               \:::\  /:::/    /       \:::\__/:::/    /       \:::\   \::/    /       \:::\  /:::/    /    ";
echo "     \:::\/:::/    /  \/____/                 \:::\/:::/    /         \::::::::/    /         \:::\   \/____/         \:::\/:::/    /     ";
echo "      \::::::/    /                            \::::::/    /           \::::::/    /           \:::\    \              \::::::/    /      ";
echo "       \::::/    /                              \::::/    /             \::::/    /             \:::\____\              \::::/    /       ";
echo "        \::/    /                                \::/    /               \::/____/               \::/    /               \::/____/        ";
echo "         \/____/                                  \/____/                 ~~                      \/____/                 ~~              ";
echo "                                                                                                                                          ";
echo "C'est fait ! Le serveur Odoo est en place et fonctionne. Spécifications:"
echo "Port: $OE_PORT"
echo "Utilisateur: $OE_USER"
echo "Utilisateur PostgreSQL: $OE_USER"
echo "Code location: $OE_USER"
echo "Dossier des addons: $OE_USER/$OE_CONFIG/addons/"
echo "Mot de passe superadmin (base de données): $OE_SUPERADMIN"
echo "Démarrer le service Odoo: sudo service $OE_CONFIG start"
echo "Arrêter le service Odoo: sudo service $OE_CONFIG stop"
echo "Redémarrer le service Odoo: sudo service $OE_CONFIG restart"
if [ $INSTALL_NGINX = "True" ]; then
  echo "Fichier de configuration Nginx: /etc/nginx/sites-available/odoo"
fi
echo "-----------------------------------------------------------"
